#pragma once

#include "ofMain.h"

#ifdef NUITRACK
#include <nuitrack/Nuitrack.h>
#include "ofxNuitrack.h"
#endif

#include "ofxGLWarper.h"
#include "ofxUI.h"
#include "ofxXmlSettings.h"
#include "ofxTimer.h"

#include "DEFINES.h"
#include "Singleton.h"

#include "grid/grid.h"
#include "botons/botoDonut.h"

#include "peces/platform.h"
#include "peces/circleSwitchHandler.h"
#include "peces/circleTrack.h"
#include "peces/towerTrack.h"

#ifdef NUITRACK
using namespace tdv::nuitrack;

class Bone {
public:
	Bone(JointType _from, JointType _to, glm::vec3 _direction) {
		from = _from;
		to = _to;
		direction = _direction;
	}

	JointType from;
	JointType to;
	glm::vec3 direction;
};
#endif

class ofApp : public ofBaseApp {
public:
	void setup();
	void update();
	void draw();
	void exit();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

#ifdef NUITRACK		
	//--------------------------------------------------------------
	// REALSENSE
	void updatePointcloud();
	void updateJoint(Joint& j);
	void drawSkeleton(Skeleton& s);
	void drawJoint(Joint& j);
	void drawBones(vector<Joint> joints);
	void drawUsers();
	void drawPointcloud();

	bool bNeedPointcloudUpdate;
	ofxnui::TrackerRef tracker;
	ofTexture rgbTex;
	ofTexture depthTex;
	glm::vec2 rgbFrameSize;
	glm::vec2 depthFrameSize;

	vector<Skeleton> skeletons;
	vector<User> users;

	ofVboMesh pc;				// pointcloud

	float cameraHeight = 1.0;	// height of camera in realspace (m)
	glm::vec3 floorPoint;
	glm::vec3 floorNormal;
#endif

	//--------------------------------------------------------------
	// APP
	Singleton* singleton; // PUNTUACIÓ jugador

	pantallesJoc pantallaJoc;
	pantallesJoc pantallaJocAnterior;
	string pantallaToString();

	//Images
	ofImage startImage;
	ofImage ropeImage;
	ofImage jumpImage;
	ofImage buttonsImage;
	ofImage towerImage;

	//Sounds
	ofSoundPlayer startSound;
	ofSoundPlayer ropeSound;
	ofSoundPlayer jumpSound;

	void setVariablesIniciPartida(); // endreçat
	void drawPuntuacio();
	void drawEnd();

	// CORNER PIN
	ofxGLWarper warper;

	// BOTONS
	botoDonut botoStart;
	botoDonut botoEsquerra;
	botoDonut botoDreta;

	// GRID
	grid myGrid;

	// TEMPS DE JOC
	ofxTimer jocMinutsTimer;
	float jocMinutsTimerSegonsLeft;
	int jocMinutsTimerMinuts;
	int jocMinutsTimerSegons;
	void drawTemps();
	ofColor saltingBlue;
	ofColor red;
	ofTrueTypeFont	saltingTypo;

	// TEMPS DE PANTALLA FINAL
	ofxTimer duradaTheEndTimer;

	//--------------------------------------------------------------
	// PUNTER
	ofImage punter;
	float punterWidthMig, punterHeightMig;

	// PECES
	int comptadorPeces;
	platform platform1;
	platform platform2;
	platform invisiblePlatform;
	circleSwitchHandler circlesHandler;
	circleTrack circleLeft;
	circleTrack circleRight;
	circleTrack circlebottom;
	towerTrack towerCenter;
	void setupCircles();
	void setupTowers();
	void checkEmptyCircles();
	void checkEmptyTowers();
	void updatePointsEmpty(int& e);
	void setupPlataforma();
	void setupCircleSwitch();
	void comprobarEstatsPecesEmpty();
	void actualitzaPuntsEmpty(int& e);

	//--------------------------------------------------------------
	// DETECCIÓ
	float relAspectWidth; // detecció
	float relAspectHeight;
	float baixaHoTotAvall; // ajust fi amunt-avall
	float mouHoTotDretaEsq; // ajust fi esquerra-dreta

	bool bshowImagesAndSkeleton = false;
	bool bshowCamera = false;

	// contorns
	float tmpX, tmpY;
	ofVec4f tmpVecBlobPos;
	ofVec2f warpMousePos;

	//blobs
	ofVec2f posicionsBlobs[MAX_NUM_BLOBS];
	int totalBlobsDetected;

	// GUI APP i DETECCIO
	ofxUICanvas* guia;

	// GUI AJUST BOTONS
	ofxUICanvas* guib;

	// GUI HELP
	ofxUICanvas* guih;
	void guiEvent(ofxUIEventArgs& e); // per a tots els GUIs

	//--------------------------------------------------------------
	// HELP INFO
	void toogleDrawInfoGrid();
	bool bdrawMouseWarped = true;

};
