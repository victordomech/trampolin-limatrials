#include "circleTrack.h"

ofEvent<int> circleTrack::updatePoints = ofEvent<int>();

circleTrack::circleTrack(){

}

circleTrack::~circleTrack(){

}

void circleTrack::setup(int id, int rad){
    circlePoints = 1;

    circleId = id;
    circlePos = ofVec2f(0,0);
    circleRad = rad;

    bCircleTrackActive = true;
	ropeBreakSound.load("sounds/ropeBreak1.mp3");

    // TIMER
    outsideTimer.setup(2 * 1000, false);
    outsideTimer.reset();
    outsideTimer.stopTimer();
}

void circleTrack::init(int id, ofVec2f pos){
    circleId = id;
    circlePos = pos;
}

void circleTrack::update(int total, ofVec2f pos[MAX_NUM_BLOBS]){
    if(bCircleTrackActive){
        // DETECTION
        for(int i=0; i<total; i++){
            if(currentState == INSIDE){
                if(!(isInside(pos[i].x, pos[i].y))){
                    currentState = TRANSITION;
                    nextState = OUTSIDE;
                }
            }
            if (currentState == OUTSIDE) {
                if (isInside(pos[i].x, pos[i].y)) {
                    currentState = TRANSITION;
                    nextState = INSIDE;
                }
            }
        }
    }

    // STATES
    if (currentState == TRANSITION){
        currentState = nextState;

        if(currentState == INSIDE){

        }
        else if(currentState == OUTSIDE){
            outsideTimer.startTimer();
        }
        else if(currentState == KILL){

        }


    }

    else if(currentState == CIRCLE_START){
        currentState = TRANSITION;
        nextState = OUTSIDE;
    }

    else if(currentState == INSIDE){

    }

    else if(currentState == OUTSIDE){
        if (outsideTimer.isTimerFinished()) {
			if (not ropeBreakSound.isPlaying()) {
				ropeBreakSound.play();
			}
            ofNotifyEvent(updatePoints, circlePoints);
            outsideTimer.reset();
            outsideTimer.startTimer();
        }
    }

    else if(currentState == KILL){
        currentState = TRANSITION;
        nextState = KILL;

    }

    else if(currentState == TRANSITION){
        
    }
}

void circleTrack::updatem(ofVec2f pos){
    // MOUSE
    if(bCircleTrackActive){
        if(currentState == INSIDE){
            if(!(isInside(pos.x, pos.y))){
                currentState = TRANSITION;
                nextState = OUTSIDE;
            }
        }
        if (currentState == OUTSIDE) {
            if (isInside(pos.x, pos.y)) {
                currentState = TRANSITION;
                nextState = INSIDE;
            }
        }
    }
}


void circleTrack::draw(){
    // HELP
    if(bdrawInfoHelp){
        ofPushStyle();
        ofSetColor(255,255,0,60);
        // radi deteccio peça
        ofNoFill();
        ofCircle(circlePos.x, circlePos.y, circleRad);
        ofDrawBitmapString(returnState(), circlePos.x, circlePos.y-10);
        ofDrawBitmapString(ofToString(circleId), circlePos.x, circlePos.y);
        ofPopStyle();
    }

    else if(currentState == CIRCLE_START){

    }

    else if(currentState == INSIDE){
		if (circleId != 2) {
			actionCount -= 1;
			if (actionCount <= 0) {
				actionCount = (int)ofRandom(10, 50);
				mov_y = rand() % 3 - rand() % 3;
				mov_s = rand() % 3 - rand() % 3;
			}
			if (mov_s_total + mov_s < 20 && mov_s_total + mov_s > -20) {
				mov_s_total = mov_s_total + mov_s;
				circlePos.x = circlePos.x + mov_s;
			}
			if (circlePos.y + mov_y > 0.7 * APP_HEIGT ||
				circlePos.y + mov_y < 0.3 * APP_HEIGT) {
				mov_y = 0;
				actionCount = 0;
			}
			circlePos.y = circlePos.y + mov_y;
		}
        drawStaticCircle(0, 255, 0);
    }

    else if(currentState == OUTSIDE){
        drawStaticCircle(255, 0, 0);
    }

    else if(currentState == KILL){

    }

    else if(currentState == TRANSITION){
        
    }

}

bool circleTrack::isInside(float x, float y){
    return (circlePos.distance(ofVec2f(x,y)) < circleRad);
}

string circleTrack::returnState(){
    if(currentState == CIRCLE_START){return "CIRCLE_START";}
    if(currentState == INSIDE){return "INSIDE";}
    if(currentState == OUTSIDE){return "OUTSIDE";}
    if(currentState == KILL){return "KILL";}
    if(currentState == TRANSITION){return "TRANSITION";}
    else{return "NOT_DEFINED";}

}

void circleTrack::toggleDrawInfoHelp(){
    bdrawInfoHelp = !bdrawInfoHelp;
}

void circleTrack::showDrawInfoHelp(){
    bdrawInfoHelp = true;
}

void circleTrack::hideDrawInfoHelp(){
    bdrawInfoHelp = false;
}

void circleTrack::drawStaticCircle(int r, int g, int b){
        ofPushStyle();
        ofSetColor(r, g , b, 60);
        ofCircle(circlePos.x, circlePos.y, circleRad);
        ofPopStyle();
}
