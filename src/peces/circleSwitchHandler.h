#pragma once

#ifndef _CIRCLESWITCHHANDLER
#define _CIRCLESWITCHHANDLER

#include "ofMain.h"
#include "DEFINES.h"
#include "circleSwitch.h"
#include <ofxTimer.h>

class circleSwitchHandler {
public:

	circleSwitchHandler();
	~circleSwitchHandler();

	static ofEvent<int> updatePoints;

	void setup();
	void update(int total, ofVec2f pos[MAX_NUM_BLOBS]);
	void updatem(ofVec2f pos);
	void draw();
	void fillArray();
	int id;

	// BOTÓ
	bool botoSeleccionat;
	int botoRadi, botoX, botoY;

	float velocitatCarrega;
	float amtCarrega360 = 0;
	int	errorPoints;

	circleSwitch circles[NUM_CIRCLESWITCH];

	ofxTimer errorTimer;

	// INFO HELP
	bool bdrawInfoHelp;
	void showDrawInfoHelp();
	void hideDrawInfoHelp();
	void toggleDrawInfoHelp();

protected:

};

#endif
