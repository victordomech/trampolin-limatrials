#include "circleSwitchHandler.h"

// the static event, or any static variable, must be initialized outside of the class definition.
ofEvent<int> circleSwitchHandler::updatePoints = ofEvent<int>();

circleSwitchHandler::circleSwitchHandler() {

}

circleSwitchHandler::~circleSwitchHandler() {

}

void circleSwitchHandler::setup() {

	fillArray();
	id = 0;
	errorPoints = 1;
	//TIME ERROR
	errorTimer.setup(2 * 1000, false); // cada 2s se suma puntos de error
	errorTimer.reset();
	errorTimer.startTimer();
}

void circleSwitchHandler::updatem(ofVec2f pos) {
	// MOUSE
		
	circles[id].updatem(pos);
	if (circles[id].botoSeleccionat) {
		id++;
	}
}

void circleSwitchHandler::update(int total, ofVec2f pos[MAX_NUM_BLOBS]) {
	// DETECCIO

	circles[id].update(total, pos);
	if (circles[id].botoSeleccionat) {
		id++;
	}

	if (errorTimer.isTimerFinished()) {
		ofNotifyEvent(updatePoints, errorPoints);
		errorTimer.reset();
		errorTimer.startTimer();
	}
}

void circleSwitchHandler::draw() {
	
	for (int i = 0; i < 5; i++) {
        if(id + i < 20){
            circles[id + i].draw();
        }
	}
}

void circleSwitchHandler::fillArray() {
	bool flag;
	int j;
	for (int i = 0; i < 20; i++){
		int randomPosX = (int)ofRandom(100, APP_WIDTH - 100);
		int randomPosY = (int)ofRandom(100, APP_HEIGT - 100);
		flag = true;
		j = 4;
		if (i < 4) {
			j = i;
		}
		while (flag==true && j != 0) {
			if (ofVec2f(randomPosX, randomPosY).distance(circles[i-j].getPos()) < 80) {
				flag = false;
			}
			j--;
		}
		if (flag==false) {
			i--;
		}
		else {
			circles[i].setup(i, randomPosX, randomPosY, 40);
		}
	}
}

void circleSwitchHandler::showDrawInfoHelp() {
	bdrawInfoHelp = true;
}

void circleSwitchHandler::hideDrawInfoHelp() {
	bdrawInfoHelp = false;
}

void circleSwitchHandler::toggleDrawInfoHelp() {
	bdrawInfoHelp = !bdrawInfoHelp;
}
