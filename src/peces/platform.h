#pragma once

#ifndef _PLATFORM
#define PLATFORM

#include "ofMain.h"
#include "DEFINES.h"
#include <ofxTimer.h>

enum platformStates {
	PLATFORM_START,
	PLATFORM_INSIDE,
	PLATFORM_OUTSIDE,
	PLATFORM_KILL,
	PLATFORM_TRANSITION,
	PLATFORM_INVISIBLE
};

class platform {
public:

	platform();
	~platform();

	static ofEvent<int> updatePoints;

	ofRectangle rectangle;

	//Functions
	void init(int id, ofVec2f pos);
	void setup(int id, ofVec2f pos, int width, int height);
	void update(int total, ofVec2f pos[MAX_NUM_BLOBS]);
	void updatem(ofVec2f pos);
	void draw();

	bool isInside(float x, float y);
	void fadeOutColor();
	bool platformGameErrorm(platform pl1, platform pl2, float x, float y);
	bool platformGameError(platform pl1, platform pl2, int _total, ofVec2f _pos[MAX_NUM_BLOBS]);
	string returnState();

	//Draw Variables
	int platformId;
	ofVec2f platformPos;
	int platformWidth;
	int platformHeight;

	//Other Variables
	bool bPlatformActive;
	int platformPoints;
	int duration;
	int transparency;
	
	//Textures
	ofTexture mTex;

	//IMAGES
	ofImage logImage;

	//SOUNDS
	ofSoundPlayer jumpBreakSound;

	//Sates
	platformStates currentState;
	platformStates nextState;

	//Timer
	ofxTimer outsideTimer;
	ofxTimer platformDurationTimer;

	// INFO HELP
	bool bdrawInfoHelp;
	void toggleDrawInfoHelp();
	void showDrawInfoHelp();
	void hideDrawInfoHelp();

};

#endif
