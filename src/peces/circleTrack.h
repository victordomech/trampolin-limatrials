#ifndef _CIRCLE_TRACK
#define _CIRCLE_TRACK

#include "ofMain.h"
#include "DEFINES.h"
#include <ofxTimer.h>

enum circleTrackStates{
    CIRCLE_START,
    INSIDE,
    OUTSIDE,
    KILL,
    TRANSITION
};

class circleTrack{
    public:
        circleTrack();
        ~circleTrack();

        static ofEvent<int> updatePoints;

        //Functions
        void init(int id, ofVec2f pos);
        void setup(int id, int radi);
        void update(int total, ofVec2f pos[MAX_NUM_BLOBS]);
        void draw();
        void drawStaticCircle(int r, int g, int b);

        bool isInside(float x, float y);
        void updatem(ofVec2f pos);
        string returnState();

		//SOUNDS
		ofSoundPlayer ropeBreakSound;

        //Draw Variables
        int circleId;
        ofVec2f circlePos;
        int circleRad;

        //Movement Variables
        int actionCount = 10;
        int mov_y = 1;
        int mov_s = 0;
        int mov_s_total = 0;

        //Other Variables
        int circlePoints;
        bool bCircleTrackActive;


        //Sates
        circleTrackStates currentState;
        circleTrackStates nextState;

        //Timer
        ofxTimer outsideTimer;

        // INFO HELP
        bool bdrawInfoHelp;
        void toggleDrawInfoHelp();
        void showDrawInfoHelp();
        void hideDrawInfoHelp();
};

#endif