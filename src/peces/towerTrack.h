#ifndef _TOWER_TRACK
#define _TOWER_TRACK

#include "ofMain.h"
#include "DEFINES.h"

enum towerTrackStates {
	TOWER_START,
	TOWER_ACTIVE,
	TOWER_KILL,
	TOWER_TRANSITION
};

class towerTrack {
public:
	towerTrack();
	~towerTrack();

	static ofEvent<int> updatePoints;

	//Functions
	void init(int id, ofVec2f pos);
	void setup(int id, int base, int height);
	void update(int total, ofVec2f pos[MAX_NUM_BLOBS]);
	void draw();
	void drawStaticTower(int r, int g, int b);
	void drawMisilesLeft();
	void drawMisilesRight();
	void leftMisileRandomHeight(ofRectangle& misile);
	void rightMisileRandomHeight(ofRectangle& misile);

	bool isInside(float x, float y);
	bool misileInside(ofRectangle& misile, float x, float y);
	void updatem(ofVec2f pos);
	string returnState();

	//Rectangle
	ofRectangle rectangle;
	ofRectangle misilesLeft[2];
	ofRectangle misilesRight[2];
	ofImage bananaLeft[2];
	ofImage bananaRight[2];
	int bananaLeftOfset[2];
	int bananaRightOfset[2];

	//Draw Variables
	int towerId;
	ofVec2f towerPos;
	int towerBase;
	int towerHeight;

	//Movement Variables
	int actionCount = 10;
	int mov_y = 1;
	int mov_s = 0;
	int mov_s_total = 0;

	//Other Variables
	int towerPoints;
	bool bTowerTrackActive;
	ofSoundPlayer explosion;
	ofSoundPlayer clap;


	//Sates
	towerTrackStates currentState;
	towerTrackStates nextState;

	// INFO HELP
	bool bdrawInfoHelp;
	void toggleDrawInfoHelp();
	void showDrawInfoHelp();
	void hideDrawInfoHelp();
};

#endif
