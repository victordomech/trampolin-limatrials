#include "towerTrack.h"

ofEvent<int> towerTrack::updatePoints = ofEvent<int>();

towerTrack::towerTrack() {

}

towerTrack::~towerTrack() {

}

void towerTrack::setup(int id, int base, int height) {
	towerPoints = 1;

	towerId = id;
	towerPos = ofVec2f(0, 0);
	towerBase = base;
	towerHeight = height;

	bTowerTrackActive = true;

	rectangle.setWidth(base);
	rectangle.setHeight(height);

	explosion.load("sounds/explosion.wav");
	clap.load("sounds/clap.wav");

	for (int i = 0; i <= 1; i += 1) {
		misilesLeft[i].setWidth(60);
		misilesLeft[i].setHeight(60);
		leftMisileRandomHeight(misilesLeft[i]);
		misilesRight[i].setWidth(60);
		misilesRight[i].setHeight(60);
		rightMisileRandomHeight(misilesRight[i]);
		bananaLeftOfset[i] = 540 * i;
		bananaRightOfset[i] = 540 * i;
		bananaLeft[i].load("elements/towerBananaRight.png");
		bananaRight[i].load("elements/towerBananaRight.png");
	}

}

void towerTrack::init(int id, ofVec2f pos) {
	towerId = id;
	towerPos = pos;
}

void towerTrack::update(int total, ofVec2f pos[MAX_NUM_BLOBS]) {
	if (bTowerTrackActive) {
		// DETECTION
		for (int i = 0; i < total; i++) {
			if (currentState == TOWER_ACTIVE) {
				for (int j = 0; j <= 1; j++) {
					if (misileInside(misilesRight[j], pos[i].x, pos[i].y)) {
						clap.play();
						rightMisileRandomHeight(misilesRight[j]);
					}
					if (misileInside(misilesLeft[j], pos[i].x, pos[i].y)) {
						clap.play();
						leftMisileRandomHeight(misilesLeft[j]);
					}
				}
			}
		}
	}

	// STATES
	if (currentState == TOWER_TRANSITION) {
		currentState = nextState;

		if (currentState == TOWER_ACTIVE) {
		}
		else if (currentState == TOWER_KILL) {
		}

	}
	else if (currentState == TOWER_START) {
		currentState = TOWER_TRANSITION;
		nextState = TOWER_ACTIVE;
	}

	else if (currentState == TOWER_ACTIVE) {
	}

	else if (currentState == TOWER_KILL) {
		currentState = TOWER_TRANSITION;
		nextState = TOWER_KILL;

	}

	else if (currentState == TOWER_TRANSITION) {

	}
}

void towerTrack::updatem(ofVec2f pos) {
	// MOUSE
	if (bTowerTrackActive) {
		if (currentState == TOWER_ACTIVE) {
			for (int j = 0; j <= 1; j++) {
				if (misileInside(misilesRight[j], pos.x, pos.y)) {
					clap.play();
					rightMisileRandomHeight(misilesRight[j]);
				}
				if (misileInside(misilesLeft[j], pos.x, pos.y)) {
					clap.play();
					leftMisileRandomHeight(misilesLeft[j]);
				}
			}
		}
	}
}


void towerTrack::draw() {
	// HELP
	if (bdrawInfoHelp) {
		ofPushStyle();
		ofSetColor(255, 255, 0);
		// radi deteccio peça
		ofNoFill();
		ofRectangle(towerPos, towerBase, towerHeight);
		ofDrawBitmapString(returnState(), towerPos.x, towerPos.y - 10);
		ofDrawBitmapString(ofToString(towerId), towerPos.x, towerPos.y);
		ofPopStyle();
	}

	else if (currentState == TOWER_START) {

	}

	else if (currentState == TOWER_ACTIVE) {
		drawStaticTower(255, 0, 0);
		drawMisilesLeft();
		drawMisilesRight();
	}

	else if (currentState == TOWER_KILL) {

	}

	else if (currentState == TOWER_TRANSITION) {

	}

}

bool towerTrack::isInside(float x, float y) {
	return (rectangle.inside(x, y));
}

bool towerTrack::misileInside(ofRectangle & misile, float x, float y) {
	return (misile.inside(x, y));

}

string towerTrack::returnState() {
	if (currentState == TOWER_START) { return "TOWER_START"; }
	if (currentState == TOWER_ACTIVE) { return "TOWER_ACTIVE"; }
	if (currentState == TOWER_KILL) { return "TOWER_KILL"; }
	if (currentState == TOWER_TRANSITION) { return "TOWER_TRANSITION"; }
	else { return "TOWER_NOT_DEFINED"; }

}

void towerTrack::toggleDrawInfoHelp() {
	bdrawInfoHelp = !bdrawInfoHelp;
}

void towerTrack::showDrawInfoHelp() {
	bdrawInfoHelp = true;
}

void towerTrack::hideDrawInfoHelp() {
	bdrawInfoHelp = false;
}

void towerTrack::leftMisileRandomHeight(ofRectangle & misile) {
	misile.setPosition(0 - (int)ofRandom(150, 400),
		(int)ofRandom(APP_HEIGT - D_TOWER_HEIGHT, APP_HEIGT - 100));
}

void towerTrack::rightMisileRandomHeight(ofRectangle & misile) {
	misile.setPosition(APP_WIDTH + (int)ofRandom(150, 400),
		(int)ofRandom(APP_HEIGT - D_TOWER_HEIGHT, APP_HEIGT - 100));
}

void towerTrack::drawStaticTower(int r, int g, int b) {
	//ofPushStyle();
	//ofSetColor(r, g , b);
	rectangle.setPosition(towerPos.x, towerPos.y);
	//ofDrawRectangle(rectangle);
	//ofPopStyle();
}

void towerTrack::drawMisilesLeft() {

	ofPushStyle();
	ofSetColor(255, 255, 255);
	int misile_x;
	int misile_y;

	for (int i = 0; i <= 1; i += 1) {
		misilesLeft[i].setPosition(misilesLeft[i].getPosition().x + 2, misilesLeft[i].getPosition().y);

		misile_x = misilesLeft[i].getTopRight().x;
		misile_y = misilesLeft[i].getTopRight().y;

		if (misile_x >= 0) {
			bananaLeft[i].drawSubsection(
				misilesLeft[i].getTopLeft().x,
				misilesLeft[i].getTopRight().y,
				60, 60,
				bananaLeftOfset[i], 0
			);
			bananaLeftOfset[i] -= 60;
			if (bananaLeftOfset[i] < 0) {
				bananaLeftOfset[i] = 1140;
			}
		}
		if (rectangle.inside(misile_x, misile_y)) {
			explosion.play();
			ofNotifyEvent(updatePoints, towerPoints);
			leftMisileRandomHeight(misilesLeft[i]);
		}
	}
	ofPopStyle();

}

void towerTrack::drawMisilesRight() {

	ofPushStyle();
	ofSetColor(255, 255, 255);
	int misile_x;
	int misile_y;

	for (int i = 0; i <= 1; i += 1) {
		misilesRight[i].setPosition(misilesRight[i].getPosition().x - 2, misilesRight[i].getPosition().y);

		misile_x = misilesRight[i].getTopLeft().x;
		misile_y = misilesRight[i].getTopLeft().y;

		if (misile_x <= APP_WIDTH) {
			//ofDrawRectangle(misilesRight[i]);
			bananaRight[i].drawSubsection(
				misilesRight[i].getTopLeft().x,
				misilesRight[i].getTopRight().y,
				60, 60,
				bananaRightOfset[i], 0
			);
			bananaRightOfset[i] += 60;
			if (bananaRightOfset[i] == 1140) {
				bananaRightOfset[i] = 0;
			}
		}

		if (rectangle.inside(misile_x, misile_y)) {
			explosion.play();
			ofNotifyEvent(updatePoints, towerPoints);
			rightMisileRandomHeight(misilesRight[i]);
		}
	}

	ofPopStyle();

}
