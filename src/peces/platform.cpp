#include "platform.h"

// the static event, or any static variable, must be initialized outside of the class definition.
ofEvent<int> platform::updatePoints = ofEvent<int>();

platform::platform() {

}

platform::~platform() {

}

void platform::setup(int id, ofVec2f pos, int width, int height){

	// PLATFORM
	platformId = id;
	platformPos = pos;

	bPlatformActive = true;
	platformPoints = 5;
	duration = 7;
	rectangle.set(pos, width, height);
	//IMAGE
	logImage.load("elements/jumpLog.png");
	//SOUND
	jumpBreakSound.load("sounds/jumpBreak.wav");
    // ESTAT
	switch (platformId) {

	case 1:

		currentState = PLATFORM_START;
		nextState = PLATFORM_START;
		break;

	case 2:

		currentState = PLATFORM_KILL;
		nextState = PLATFORM_KILL;
		break;

	case 3:

		currentState = PLATFORM_INVISIBLE;
		nextState = PLATFORM_INVISIBLE;
		break;

	default: 
		cout << "platform without id";
	}

    bdrawInfoHelp = false;
	transparency = 255;

	//TIME ERROR
    outsideTimer.setup(2 * 1000, false); // cada 2s se suma puntos de error
    outsideTimer.reset();
    outsideTimer.stopTimer();

	platformDurationTimer.setup(duration * 1000, false); // duración de la plataforma 
	platformDurationTimer.reset();
	platformDurationTimer.stopTimer();
}



void platform::init(int id, ofVec2f pos){
	platformId = id;
	platformPos = pos;
}

bool platform::isInside(float _x, float _y){
	return rectangle.inside(_x, _y);
}

void platform::update(int _total, ofVec2f _pos[MAX_NUM_BLOBS]){
	if (bPlatformActive) {

		if (platformDurationTimer.isTimerFinished()) {
			if (currentState != PLATFORM_KILL) {
				currentState = PLATFORM_KILL;
				nextState = PLATFORM_KILL;
			}
		}
		else {
			for (int i = 0; i < _total; i++) {
				if (currentState == PLATFORM_START) {
					if (!(isInside(_pos[i].x, _pos[i].y))) {
						currentState = PLATFORM_TRANSITION;
						nextState = PLATFORM_OUTSIDE;
					}
					else if (isInside(_pos[i].x, _pos[i].y)) {
						currentState = PLATFORM_TRANSITION;
						nextState = PLATFORM_INSIDE;
					}
				}
				else if (currentState == PLATFORM_INSIDE) {
					if (!(isInside(_pos[i].x, _pos[i].y))) {
						currentState = PLATFORM_TRANSITION;
						nextState = PLATFORM_OUTSIDE;
					}
				}
				else if (currentState == PLATFORM_OUTSIDE) {
					if (isInside(_pos[i].x, _pos[i].y)) {
						currentState = PLATFORM_TRANSITION;
						nextState = PLATFORM_INSIDE;
					}
				}
			}
		}
	}

    // ESTATS PLATFORM
    if(currentState == PLATFORM_TRANSITION){
		platformDurationTimer.startTimer();
        if(nextState == PLATFORM_INSIDE){
			currentState = PLATFORM_INSIDE;
        }
        else if (nextState == PLATFORM_OUTSIDE) {
			currentState = PLATFORM_OUTSIDE;
        }       
		else if (nextState == PLATFORM_KILL) {
			currentState = PLATFORM_KILL;
		}
    }

    else if (currentState == PLATFORM_OUTSIDE) {
    }

	else if (currentState == PLATFORM_KILL) {

		if (platformDurationTimer.isTimerFinished()) {
			rectangle.set(platformPos, 0, 0);
			platformDurationTimer.setup((duration - 2) * 1000, false);
			platformDurationTimer.reset();
			platformDurationTimer.startTimer();
		}

		else if (platformDurationTimer.getTimeLeftInSeconds() < 2) {
			int randomPos = (int)ofRandom(0, APP_WIDTH - 175);
			ofVec2f posPlatform = ofVec2f(randomPos, APP_HEIGT - 30);
			setup(1, posPlatform, 175, 20);
			platformDurationTimer.reset();
			platformDurationTimer.startTimer();
		}

		else if (platformDurationTimer.getTimeLeftInSeconds() == duration) {
			platformDurationTimer.reset();
			platformDurationTimer.startTimer();
		}
	}
}

void platform::updatem(ofVec2f _pos) {
	// MOUSE
	if (bPlatformActive) {

		if (platformDurationTimer.isTimerFinished()) {
		}
		else if (currentState == PLATFORM_START) {
			if (!(isInside(_pos.x, _pos.y))) {
				currentState = PLATFORM_TRANSITION;
				nextState = PLATFORM_OUTSIDE;
			}
			else if (isInside(_pos.x, _pos.y)) {
				currentState = PLATFORM_TRANSITION;
				nextState = PLATFORM_INSIDE;
			}
		}

		else if (currentState == PLATFORM_INSIDE) {
			if (!(isInside(_pos.x, _pos.y))) {
				currentState = PLATFORM_TRANSITION;
				nextState = PLATFORM_OUTSIDE;
			}
		}
		else if (currentState == PLATFORM_OUTSIDE) {
			if (isInside(_pos.x, _pos.y)) {
				currentState = PLATFORM_TRANSITION;
				nextState = PLATFORM_INSIDE;
			}
		}
	}  
}

void platform::draw(){

	fadeOutColor();
	
    if(currentState == PLATFORM_START){

		ofPushStyle();
		ofSetColor(ofColor(255, 255, 255));
		//ofDrawRectangle(rectangle);
		logImage.draw(rectangle.getTopLeft().x, rectangle.getTopLeft().y - 20);
		ofPopStyle();
    }
    else if(currentState == PLATFORM_INSIDE){
		ofPushStyle();
		ofSetColor(ofColor(255, 255, 255, transparency));
		logImage.draw(rectangle.getTopLeft().x, rectangle.getTopLeft().y - 20);
		ofPopStyle();

    }
    else if(currentState == PLATFORM_OUTSIDE){
        ofPushStyle();
        ofSetColor(ofColor(255, 255, 255, transparency));
        //ofDrawRectangle(rectangle);
		logImage.draw(rectangle.getTopLeft().x, rectangle.getTopLeft().y - 20);
        ofPopStyle();
    }

    else if(currentState == PLATFORM_KILL){
    }

	else if (currentState == PLATFORM_INVISIBLE) {

		ofPushStyle();
		ofSetColor(ofColor(255, 255, 255, 25));
		//ofDrawRectangle(rectangle);
		logImage.draw(rectangle.getTopLeft().x, rectangle.getTopLeft().y - 20);
		ofPopStyle();
	}
}


bool platform::platformGameErrorm(platform pl1, platform pl2, float x, float y) {

	if (this->isInside(x, y)) { 

		if (!(pl1.isInside(x, y)) && !(pl2.isInside(x, y))) {   // outside of the 2 platforms = ERROR
			outsideTimer.startTimer();
			if (outsideTimer.isTimerFinished()) {
				if (not jumpBreakSound.isPlaying()) {
					jumpBreakSound.play();
				}
				ofNotifyEvent(updatePoints, platformPoints);
				outsideTimer.reset();
				outsideTimer.startTimer();
				return true;
			}
		}
		else {
			outsideTimer.stopTimer();
			return false;
		}
	}
	else {  
		outsideTimer.stopTimer();
		return false;
	}
    return false;
}


bool platform::platformGameError(platform pl1, platform pl2, int _total, ofVec2f _pos[MAX_NUM_BLOBS]) {

	bool flag = false;

	for (int i = 0; i < _total; i++) {
		if (this->isInside(_pos[i].x, _pos[i].y)) { // inside invidible platform floor

			if (!(pl1.isInside(_pos[i].x, _pos[i].y)) && !(pl2.isInside(_pos[i].x, _pos[i].y))) {   // outside of the 2 platforms = ERROR
				flag = true;
			}
		}
	}
	if (flag) {
		outsideTimer.startTimer();
		if (outsideTimer.isTimerFinished()) {
			if (not jumpBreakSound.isPlaying()) {
				jumpBreakSound.play();
			}
			ofNotifyEvent(updatePoints, platformPoints);
			outsideTimer.reset();
			outsideTimer.startTimer();
		}
	}
	else {
		outsideTimer.stopTimer();
	}
	return flag;
}

void platform::toggleDrawInfoHelp() {
	bdrawInfoHelp = !bdrawInfoHelp;
}

void platform::showDrawInfoHelp() {
	bdrawInfoHelp = true;
}

void platform::hideDrawInfoHelp() {
	bdrawInfoHelp = false;
}

void platform::fadeOutColor() {
	if(platformDurationTimer.getTimeLeftInSeconds() <  2) {
		transparency -= 2;
	}
}
