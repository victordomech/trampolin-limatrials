#pragma once

#ifndef _CIRCLESWITCH
#define _CIRCLESWITCH

#include "ofMain.h"
#include "DEFINES.h"

class circleSwitch {
public:

	circleSwitch();
	~circleSwitch();

	void setup(int _id, int _x, int _y, int _radius);
	void update(int total, ofVec2f pos[MAX_NUM_BLOBS]);
	void updatem(ofVec2f pos);
	void draw();
	void init();
	void actualitzaAmt(float _x, float _y);
	int id = id;
	ofTrueTypeFont	saltingTypo;
	ofImage fruitImage;
	ofSoundPlayer fruitEat;

	bool isInside(float _x, float _y);
	ofVec2f getPos();


	// BOTÓ
	bool botoSeleccionat;
	int botoRadi, botoX, botoY;

	float velocitatCarrega;
	float amtCarrega360 = 0;

	// INFO HELP
	bool bdrawInfoHelp;
	void showDrawInfoHelp();
	void hideDrawInfoHelp();
	void toggleDrawInfoHelp();

protected:

};

#endif
