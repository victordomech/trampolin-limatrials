#include "circleSwitch.h"

circleSwitch::circleSwitch() {

}

circleSwitch::~circleSwitch() {

}

void circleSwitch::setup(int _id, int _x, int _y, int _radius) {
	// BOTÓ
	bdrawInfoHelp = true;
	botoSeleccionat = false;
	velocitatCarrega = 10;
	botoRadi = _radius;
	botoX = _x;
	botoY = _y;
	id = _id;
	saltingTypo.load("Oswald-Bold.otf", 27, true, true); // temps nivell i normal, kids, pro
	saltingTypo.setLetterSpacing(1.2);
	fruitImage.load("elements/fruit" + std::to_string((int)ofRandom(1, 4)) + ".png");
	fruitEat.load("sounds/fruitEat.mp3");
	// CÀRREGA
	amtCarrega360 = 0;
}

void circleSwitch::init() {
	amtCarrega360 = 0;
	botoSeleccionat = false;
}

void circleSwitch::updatem(ofVec2f pos) {
	// MOUSE
	actualitzaAmt(pos.x, pos.y);
}

void circleSwitch::update(int total, ofVec2f pos[MAX_NUM_BLOBS]) {
	// DETECCIO
	for (int i = 0; i < total; i++) {
		actualitzaAmt(pos[i].x, pos[i].y);
	}
}

void circleSwitch::draw() {
	if (botoSeleccionat) {

		ofPushStyle();
		ofSetColor(255, 255, 255);
		//ofCircle(botoX, botoY, botoRadi);
		fruitImage.draw(botoX - botoRadi, botoY - botoRadi);
		ofPopStyle();
	}
	else {

		ofPushStyle();
		ofSetColor(255, 255, 255);
		ofNoFill();
		//ofCircle(botoX, botoY, botoRadi);
		fruitImage.draw(botoX - botoRadi, botoY - botoRadi);
		string s = ofToString(id + 1);
		saltingTypo.drawString(s, botoX - 8, botoY  + 13);
		ofPopStyle();
	}
}

void circleSwitch::actualitzaAmt(float _x, float _y) {
	if (isInside(_x, _y)) {
		amtCarrega360 += velocitatCarrega;
		if (amtCarrega360 >= 360) {
			amtCarrega360 = 0;
			botoSeleccionat = true;
			fruitEat.play();
		}
	}
}

bool circleSwitch::isInside(float _x, float _y) {
	return (ofVec2f(_x, _y).distance(ofVec2f(botoX, botoY)) < botoRadi);
}

ofVec2f circleSwitch::getPos() {

	ofVec2f pos = ofVec2f(botoX,botoY);
	return pos;
}

void circleSwitch::showDrawInfoHelp() {
	bdrawInfoHelp = true;
}

void circleSwitch::hideDrawInfoHelp() {
	bdrawInfoHelp = false;
}

void circleSwitch::toggleDrawInfoHelp() {
	bdrawInfoHelp = !bdrawInfoHelp;
}
